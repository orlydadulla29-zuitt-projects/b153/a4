let params = new URLSearchParams(window.location.search)

let courseId = params.get("courseId")

//console.log(courseId)

/*
Activity:

Using fetch, show the details of the page's specific course in the console

*/

fetch(`http://localhost:4000/courses/${courseId}`)
.then(res => res.json())
.then(data => {
	console.log(data)

	let courseName = document.querySelector("#courseName")
	courseName.innerHTML =

	`
	<h2 id="courseName" class="mt-5">${data.name}</h2>
	`

	let coursePrice = document.querySelector("#coursePrice")

	coursePrice.innerHTML =

	`
	<h2 id="coursePrice" class="mt-5">${data.price}</h2>
	`
})